import React, {useState} from "react";
import axios from "axios";

function Form() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [tel, setTel] = useState('');

    const handleFirstNameChange = (e) => {
        setFirstName(e.target.value)
    }
    const handleLastNameChange = (e) => {
        setLastName(e.target.value)
    }
    const handleEmailChange = (e) => {
        setEmail(e.target.value)
    }
    const handleAddressChange = (e) => {
        setAddress(e.target.value)
    }
    const handlePhoneChange = (e) => {
        setTel(e.target.value)
    }
    const handleSubmit = (event) => {
        axios.get("http://localhost:8000/api/nouveau")
    }
    return(
        <form onSubmit={handleSubmit}>
            <label>
                Nom :
                <input type="text" name="nom" onChange={handleFirstNameChange}/><br />
                Prénom :
                <input type="text" name="prenom" onChange={handleLastNameChange}/><br />
                Email :
                <input type="text" name="email" onChange={handleEmailChange}/><br />
                Adresse :
                <input type="text" name="adresse" onChange={handleAddressChange}/><br />
                Téléphonne :
                <input type="text" name="tel" onChange={handlePhoneChange}/><br />
                Né(e) le :
                <input type="date" name="birthday"/><br />
            </label>
            <input type="submit" value="Envoyer" />
        </form>
    );
}

export default Form;