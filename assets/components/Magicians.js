import React from "react";
import axios from 'axios';
import {Link, NavLink} from "react-router-dom";
import ReactModal from 'react-modal';
import Form from "./Form"

class Magicians extends React.Component {
    constructor() {
        super();

        this.state = {magicians:[], loading:true, isOpen:false, str:"FALSE"}
        this.handleDelete = this.handleDelete.bind(this);
        this.openModal = this.openModal.bind(this);
    }

    componentDidMount() {
        axios.get('http://localhost:8000/api/magicians').then(res => {
            this.setState({magicians : res.data, loading:false})
        });
    }

    handleDelete(id) {
       axios.get('http://localhost:8000/api/delete/'+id);
       this.setState({magicians : this.state.magicians.filter(magician => magician.id !== id)});
    }

    openModal = () => {
        this.setState({isOpen: true})
    }

    render() {
        const loading = this.state.loading;
        return (
            <div>
                {
                    loading ? (
                                <div className={'row text-center'}>
                                    <span className="fa fa-spin fa-spinner fa-4x"></span>
                                </div>
                            ) :
                    (<div>
                        <button className={"btn btn-default"} onClick={this.openModal}>Ajouter magicien</button>
                        <ReactModal
                            isOpen={this.state.isOpen}
                            //onAfterOpen={afterOpenModal}
                            //onRequestClose={closeModal}
                            //style={customStyles}
                            contentLabel="Example Modal"
                        >
                            {<Form />}
                        </ReactModal>
                            <div className={'row'}>
                                { this.state.magicians.map(magician =>
                                    <div className="col-md-10 offset-md-1 row-block" key={magician.id}>
                                        <ul id="sortable">
                                            <li>
                                                <div className="media">
                                                    <div className="media-body">
                                                        <h4>{magician.lastName}</h4>
                                                        <p>{magician.firstName}</p>
                                                    </div>
                                                    <div className="media-right align-self-center">
                                                        <NavLink
                                                            style={{ display: "block", margin: "1rem 0" }}
                                                            to={`/magician/${magician.id}`}
                                                            key={"info"+magician.number}
                                                        >
                                                            Plus d'Infos
                                                        </NavLink>
                                                        <button className="btn btn-default"
                                                                onClick={()=>this.handleDelete(magician.id)}>
                                                            Delete
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                )}
                            </div>
                    </div>)
                }

            </div>

        )
    }
}

export default Magicians