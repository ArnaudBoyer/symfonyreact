<?php

namespace App\Controller;

use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Magiciens;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Serializer\SerializerInterface;
class MagiciansController extends AbstractController
{

    /**
     * @Route("/api/magicians", name="magicians")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */

    public function getMagicians(ManagerRegistry $doctrine, SerializerInterface $serializer): \Symfony\Component\HttpFoundation\JsonResponse|Response
    {
        $repo = $doctrine->getRepository(Magiciens::class);
        $magicians = $repo->findAll();
        $json = $serializer->serialize($magicians, 'json');
        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        $response->setContent($json);
        return $response;
    }

    /**
     * @Route("/api/delete/{id}", name="delete")
     */
    public function deleteMagicians($id)  {
        $bdd = new PDO('mysql:host=127.0.0.1;port=8889;dbname=magic;charset=utf8', 'root', 'root');
        echo $id;
        $bdd->query('DELETE FROM magicians WHERE id='.$id);
        return new Response();
    }

    /**
     * @Route("/api/nouveau", name="add")
     */
    public function addMagicians(Request $request) {
        $bdd = new PDO('mysql:host=127.0.0.1;port=8889;dbname=magic;charset=utf8', 'root', 'root');
        $sql = "INSERT INTO magicians (Nom, Prenom, email, adresse, tel, birthday) VALUES (?,?,?,?,?,?)";
        $bdd->prepare($sql)->execute(array_values($request->query->all()));
        return new Response();
    }

    /**
     * @Route("/api/infos/{id}", name="info")
     */
    public function getInfos(ManagerRegistry $doctrine, SerializerInterface $serializer, int $id) {
        $repo = $doctrine->getRepository(Magiciens::class);
        $magician = $repo->findBy(["id" => $id]);
        $json = $serializer->serialize($magician, 'json');
        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        $response->setContent($json);
        dd($response);
        return new Response();
    }
}